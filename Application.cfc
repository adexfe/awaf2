component output="true" {

    this.name = "awaf";
    this.applicationTimeout = createTimeSpan(0,1,0,0);
    this.clientmanagement= "yes";
    this.ClientStorage = "cookie";
    this.sessionmanagement = "yes";
    this.sessiontimeout = createTimeSpan(0,1,0,0);
    this.setClientCookies = "yes";
    this.datasource = "awaf";

    public boolean function onApplicationStart() { 

        return true;
    }

    public boolean function onRequest(required string targetPage)   {
 
        param name="url.page" default="home";  

        url.page = Replace(url.page,'.','/','all');

        include lcase(arguments.targetPage);

        return true;
    }

}