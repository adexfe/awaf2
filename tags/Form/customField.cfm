<cfoutput>

  <cfif ThisTag.ExecutionMode == "Start">
  
    <cfparam name="Attributes.TagName" type="string" default="CustomField"/>
    <cfparam name="Attributes.total" type="numeric" default="9"/>

    <cfparam name="Attributes.label" type="string" default="{Custom Field}"/>
    <cfparam name="Attributes.readonly" type="boolean" default="false"/>

    <cfparam name="Attributes.id" type="string" default="#application.fn.GetRandomVariable()#"/>

    <cfparam name="Attributes.key" type="string" default="0"/>
    <cfparam name="Attributes.modelId" type="numeric"/>
    <cfparam name="Attributes.tenantId" type="numeric"/>

    <cfset ArrayAppend(request.form.cfield, Attributes)/>
  
    <cfset cf_xform = getBaseTagData("cf_xform").Attributes/>
  
  <cfelse>
    
    <cfquery name="qC">
      SELECT * FROM custom_field_data 
      WHERE TenantId = #attributes.tenantid# 
        AND ModelId = #attributes.modelId#
        AND Key1 = "#Attributes.key#"
    </cfquery>

    <div class="row">
      <cfloop from="1" to="#Attributes.total#" index="i">
        <div class="col-sm-6 col-md-4">
          <div class="form-group ">
            <cfset label = Attributes.label/>
            <cfif qC.Name[i] != "">
              <cfset label = qC.Name[i]/>
            </cfif>
            <label class="" for="#Attributes.id##i#">#label# #i#</label>
            <div class="#Attributes.id##i#">
              <input type="text" 
                <cfif Attributes.readonly>readonly</cfif>
                value="#qC.value[i]#" 
                class="form-control" 
                name="cfield_value_#i#" 
                id="#Attributes.id##i#"/>
            </div>
          </div>
          <input type="hidden" value="#label#" class="form-control" name="cfield_label_#i#" id="#Attributes.id##i#label"/>
          <input type="hidden" value="#qC.CustomFieldDataId[i]#" class="form-control" name="cfield_pk_#i#" id="#Attributes.id##i#label"/>
        </div>
      </cfloop>
    </div>
  </cfif>
</cfoutput>